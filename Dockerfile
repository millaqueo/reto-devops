FROM node:12-alpine
ENV REPO_HOME=/app
WORKDIR ${REPO_HOME}

COPY . .

RUN npm install

RUN addgroup -S cmillaqueo && \
    adduser -s /bin/bash -S -G cmillaqueo cmillaqueo
USER cmillaqueo

CMD [ "node", "index.js"]
