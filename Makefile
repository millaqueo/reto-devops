all: stop_docker build_docker deploy_Kubernetes test_webapp_k8s terraform_apply 

stop_docker:

	docker-compose down

build_docker:

	docker-compose up -d --build --force-recreate

deploy_Kubernetes:

	kubectl apply -f kubernetes

test_webapp_k8s:

	kubectl expose deployment node-app --type=NodePort --port=80

terraform_apply:

	terraform init && terraform plan && terraform apply --auto-approve
